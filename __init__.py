# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationDailyEndTime,
        module='production_daily_end_time', type_='model')
